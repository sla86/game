## Requirements

# Components
<b>game</b> - Play game</br>
<b>gift</b> - Process rewards for winners

```shell
jdk 17, Docker, Docker Compose, internet connection
```

## Build

```shell
./mvnw clean install -f ./rabbitmq-infra/pom.xml && ./mvnw clean install -f ./game/pom.xml && ./mvnw clean install -f ./gift/pom.xml
```

## Run

```shell
docker-compose up
```

## Swagger

```shell
http://localhost:8080/gambling/swagger-ui/index.html#/
```

## Spring-boot-admin

```shell
http://localhost:8090/
```


## Usage

#### Play game
```shell
curl -X POST "http://localhost:8080/gambling/api/v1/games" -H "accept: application/json" -H "Content-Type: application/json" -d "{ \"playerId\": 123, \"shape\": \"PAPER\"}"
```

#### Check statistics
```shell
curl -X GET "http://localhost:8080/gambling/api/v1/games/123/statistics" -H "accept: application/json"
```
