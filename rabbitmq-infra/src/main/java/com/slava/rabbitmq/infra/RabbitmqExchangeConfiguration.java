package com.slava.rabbitmq.infra;

import org.springframework.amqp.core.TopicExchange;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.messaging.converter.MessageConverter;

@Configuration
public class RabbitmqExchangeConfiguration {

    public static final String GIFT_REQUEST_EXCHANGE_NAME = "gift.request";

    @Bean
    public TopicExchange snapshotPerformedChannelExchange() {
        return new TopicExchange(GIFT_REQUEST_EXCHANGE_NAME);
    }

    /*
    * check org.springframework.cloud.function.context.config.ContextFunctionCatalogAutoConfiguration.isConverterEligible(...) version: 3.4.1
    * */
    @Bean
    public MessageConverter messageConverter() {
        return new ProtoMessageConverter();
    }

}
