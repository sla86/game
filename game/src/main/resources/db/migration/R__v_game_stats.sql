CREATE OR REPLACE VIEW v_game_stats AS
SELECT one_player_id AS player_id,
       count(*) AS games,
       coalesce(sum(CASE WHEN winer_player_id = one_player_id THEN 1 ELSE 0 END), 0) AS wins,
       coalesce(sum(CASE WHEN winer_player_id = another_player_id THEN 1 ELSE 0 END), 0) AS losts,
       coalesce(sum(CASE WHEN winer_player_id is null THEN 1 ELSE 0 END), 0) AS draws
FROM t_game
WHERE one_player_id IN (one_player_id, another_player_id)
GROUP BY one_player_id
UNION
SELECT another_player_id AS player_id,
       count(*) AS games,
       coalesce(sum(CASE WHEN winer_player_id = another_player_id THEN 1 ELSE 0 END), 0) AS wins,
       coalesce(sum(CASE WHEN winer_player_id = one_player_id THEN 1 ELSE 0 END), 0) AS losts,
       coalesce(sum(CASE WHEN winer_player_id is null THEN 1 ELSE 0 END), 0) AS draws
FROM t_game
WHERE another_player_id IN (one_player_id, another_player_id)
GROUP BY another_player_id