CREATE TABLE t_game
(
    id bigint PRIMARY KEY,
    one_player_id bigint NOT NULL,
    one_player_shape varchar(10) NOT NULL,
    another_player_id bigint NOT NULL,
    another_player_shape varchar(10) NOT NULL,
    winer_player_id bigint,
    play_datetime timestamp NOT NULL
);

CREATE SEQUENCE seq_game_id
    INCREMENT BY 10
    MINVALUE 1
    CACHE 1
    NO CYCLE;