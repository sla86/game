package com.slava.game.application.service;

import java.util.Random;

import com.slava.game.application.ValidationException;
import com.slava.game.application.port.in.PlayHumanVsAiUseCase;
import com.slava.game.application.port.out.GiftNotification;
import com.slava.game.application.port.out.PersistencePort;
import com.slava.game.domain.Game;
import com.slava.game.domain.PlayerDecision;
import com.slava.game.domain.Shape;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import static com.slava.game.domain.Shape.ROCK;

@Service
@Slf4j
@RequiredArgsConstructor
public final class PlayHumanVsAiService implements PlayHumanVsAiUseCase {

    private static final Long AI_USER_ID = -1L;

    private final Random random = new Random();

    private final PersistencePort persistencePort;
    private final GiftNotification giftNotification;

    public String play(Long playerId, String shape) {
        validate(playerId);
        var s = Shape.valueOf(shape);
        var aiDecision = getAiDecision();
        var game = new Game(
            new PlayerDecision(playerId, s),
            new PlayerDecision(AI_USER_ID, aiDecision)
        );

        game.play();
        persistencePort.save(game);

        log.debug("Played [{}]", game);

        return generateResult(game, aiDecision);
    }

    private void validate(Long playerId) {
        if(playerId < 1) {
            throw new ValidationException("Invalid playerId: " + playerId );
        }
    }

    private Shape getAiDecision() {
        var ordinal = random.nextInt(0, 3);
        return Shape.values()[ordinal];
    }

    private String generateResult(Game game, Shape aiDecision) {
        if (game.getWiner() == null) {
            return "draw";
        }

        if(game.getWiner().equals(AI_USER_ID)) {
            return "You lost because of " + aiDecision.name();
        }

        giftNotification.sendGift(game.getWiner());
        return "You beat " + aiDecision.name();
    }

}
