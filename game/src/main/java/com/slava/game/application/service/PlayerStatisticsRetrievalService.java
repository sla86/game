package com.slava.game.application.service;

import com.slava.game.application.EntityNotFoundException;
import com.slava.game.application.port.in.PlayerStatisticsRetrievalUseCase;
import com.slava.game.application.port.out.PersistencePort;
import com.slava.game.domain.PlayerStatistic;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
public final class PlayerStatisticsRetrievalService implements PlayerStatisticsRetrievalUseCase {

    private final PersistencePort persistencePort;

    @Override
    public PlayerStatistic getStatistic(Long playerId) {
        return persistencePort.getStats(playerId)
            .orElseThrow(() -> new EntityNotFoundException(PlayerStatistic.class, playerId));
    }
}
