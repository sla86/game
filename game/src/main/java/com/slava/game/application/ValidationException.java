package com.slava.game.application;

public class ValidationException extends RuntimeException {

    public ValidationException(String message) {
        super(message);
    }
}
