package com.slava.game.application.port.in;

public interface PlayHumanVsAiUseCase {

    String play(Long playerId, String shape);

}
