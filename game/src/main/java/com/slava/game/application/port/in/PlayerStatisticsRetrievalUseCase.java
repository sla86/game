package com.slava.game.application.port.in;

import com.slava.game.domain.PlayerStatistic;

public interface PlayerStatisticsRetrievalUseCase {

    PlayerStatistic getStatistic(Long playerId);

}
