package com.slava.game.application.port.out;

import java.util.Optional;

import com.slava.game.domain.Game;
import com.slava.game.domain.PlayerStatistic;

public interface PersistencePort {

    void save(Game game);

    Optional<PlayerStatistic> getStats(Long playerId);
}
