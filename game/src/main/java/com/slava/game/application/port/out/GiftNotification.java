package com.slava.game.application.port.out;

public interface GiftNotification {

    void sendGift(Long playerId);
}
