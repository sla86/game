package com.slava.game.application;

import lombok.RequiredArgsConstructor;

@RequiredArgsConstructor
public class EntityNotFoundException extends RuntimeException {

    private final Class<?> entityType;
    private final Long entityIdentifier;

    @Override
    public String getMessage() {
        return "Entity type=[" + entityType.getSimpleName() + "] with identifier=[" + entityIdentifier + "] not found";
    }
}
