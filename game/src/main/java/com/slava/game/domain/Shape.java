package com.slava.game.domain;

public enum Shape {
    ROCK, PAPER, SCISSORS;
}
