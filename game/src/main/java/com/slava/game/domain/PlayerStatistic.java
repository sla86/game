package com.slava.game.domain;

public record PlayerStatistic(Long playerId, Long games, Long wins, Long losts, Long draws) {
}
