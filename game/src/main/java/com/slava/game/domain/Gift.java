package com.slava.game.domain;

public record Gift (Long playerId) {}
