package com.slava.game.domain;

public record PlayerDecision(Long playerId, Shape shape) {
}
