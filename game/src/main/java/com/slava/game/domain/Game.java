package com.slava.game.domain;

import java.util.Map;

import lombok.Getter;
import lombok.RequiredArgsConstructor;
import lombok.ToString;

import static com.slava.game.domain.Shape.PAPER;
import static com.slava.game.domain.Shape.ROCK;
import static com.slava.game.domain.Shape.SCISSORS;
import static java.util.Map.entry;

@RequiredArgsConstructor
@ToString
@Getter
public final class Game {

    private static final Map<Shape, Shape> gameRule = Map.ofEntries(
        entry(ROCK, SCISSORS),
        entry(SCISSORS, PAPER),
        entry(PAPER, ROCK)
    );

    private final PlayerDecision onePlayerDecision;
    private final PlayerDecision anotherPlayerDecision;
    private Long winer;

    public void play() {
        winer = determineWiner(onePlayerDecision, anotherPlayerDecision);
    }

    private Long determineWiner(PlayerDecision onePlayerDecision, PlayerDecision anotherPlayerDecision) {
        if (onePlayerDecision.shape() == anotherPlayerDecision.shape()) {
            return null;
        }

        return gameRule.get(onePlayerDecision.shape()) == anotherPlayerDecision.shape()
               ? onePlayerDecision.playerId()
               : anotherPlayerDecision.playerId();

    }

}
