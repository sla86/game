package com.slava.game.adapters.in.rest;

import com.slava.game.adapters.in.rest.dto.GetStatisticsResponse;
import com.slava.game.domain.PlayerStatistic;
import org.mapstruct.Mapper;
import org.mapstruct.factory.Mappers;

@Mapper(componentModel = "spring")
public interface RestMapper {

    RestMapper INSTANCE = Mappers.getMapper(RestMapper.class);

    GetStatisticsResponse playerStatisticToGetStatisticsResponse(PlayerStatistic statistic);
}
