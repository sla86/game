package com.slava.game.adapters.out.messaging;

import com.slava.game.application.port.out.GiftNotification;
import com.slava.proto.game.GiftRequest;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.cloud.stream.function.StreamBridge;
import org.springframework.stereotype.Service;
import org.springframework.util.MimeType;

@Service
@RequiredArgsConstructor
public final class GiftNotificationProducer implements GiftNotification {

    private static final MimeType MIME_TYPE = MimeType.valueOf("application/x-protobuf");

    @Value("${spring.cloud.stream.function.bindings.giftRequest-out-0}")
    private final String outputName;
    private final StreamBridge streamBridge;

    @Override
    public void sendGift(Long playerId) {
        var giftRequest = GiftRequest.newBuilder()
            .setPlayerid(playerId)
            .build();

        streamBridge.send(outputName, giftRequest, MIME_TYPE); //Exchange name also works here
    }
}
