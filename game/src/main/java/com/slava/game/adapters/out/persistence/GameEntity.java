package com.slava.game.adapters.out.persistence;

import java.time.LocalDateTime;
import javax.persistence.Entity;
import javax.persistence.EntityListeners;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.annotation.Immutable;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;

@Entity
@Immutable
@Table(name = "t_game")
@EntityListeners(AuditingEntityListener.class)
@NoArgsConstructor
@AllArgsConstructor
@Builder
@Getter
@Setter
final class GameEntity {

    @Id
    @SequenceGenerator(
        name = "game_id_generator",
        sequenceName = "seq_game_id",
        allocationSize = 10)
    @GeneratedValue(
        strategy = GenerationType.SEQUENCE,
        generator = "game_id_generator")
    private Long id;
    private Long onePlayerId;
    private String onePlayerShape;
    private Long anotherPlayerId;
    private String anotherPlayerShape;
    private Long winerPlayerId;
    @CreatedDate
    private LocalDateTime playDatetime;
}

