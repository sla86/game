package com.slava.game.adapters.out.persistence;

import com.slava.game.domain.Game;
import com.slava.game.domain.PlayerStatistic;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.factory.Mappers;

@Mapper(componentModel = "spring")
interface PersistenceMapper {

    PersistenceMapper INSTANCE = Mappers.getMapper(PersistenceMapper.class);

    @Mapping(target = "onePlayerId", source = "game.onePlayerDecision.playerId")
    @Mapping(target = "onePlayerShape", source = "game.onePlayerDecision.shape")
    @Mapping(target = "anotherPlayerId", source = "game.anotherPlayerDecision.playerId")
    @Mapping(target = "anotherPlayerShape", source = "game.anotherPlayerDecision.shape")
    @Mapping(target = "winerPlayerId", source = "winer")
    GameEntity gameToGameEntity(Game game);

    PlayerStatistic playerStatisticEntityToPlayerStatistic(PlayerStatisticEntity playerStatisticEntity);
}
