package com.slava.game.adapters.out.persistence;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
interface GameRepository extends CrudRepository<GameEntity, Long> {
}
