package com.slava.game.adapters.in.rest;

import java.io.IOException;
import javax.servlet.http.HttpServletResponse;

import com.slava.game.application.EntityNotFoundException;
import com.slava.game.application.ValidationException;
import lombok.extern.slf4j.Slf4j;
import org.springframework.core.Ordered;
import org.springframework.core.annotation.Order;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

@Slf4j
@Order(Ordered.HIGHEST_PRECEDENCE)
@ControllerAdvice
final class RestExceptionHandler extends ResponseEntityExceptionHandler {

    private static final String WARN_MESSAGE = "Failure:";

    @ExceptionHandler(EntityNotFoundException.class)
    public void handleEntityNotFoundException(EntityNotFoundException e, HttpServletResponse r) throws IOException {

        log.warn(WARN_MESSAGE, e);
        r.sendError(HttpStatus.NOT_FOUND.value(), e.getMessage());
    }

    @ExceptionHandler(ValidationException.class)
    public void handleValidationException(ValidationException e, HttpServletResponse r) throws IOException {
        log.warn(WARN_MESSAGE, e);
        r.sendError(HttpStatus.BAD_REQUEST.value(), e.getMessage());
    }

    @ExceptionHandler(Exception.class)
    public void handleException(EntityNotFoundException e, HttpServletResponse r) throws IOException {
        log.warn(WARN_MESSAGE, e);
        r.sendError(HttpStatus.INTERNAL_SERVER_ERROR.value(), ":P");
    }

}
