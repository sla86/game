package com.slava.game.adapters.in.rest;

import com.slava.game.adapters.in.rest.dto.GetStatisticsResponse;
import com.slava.game.adapters.in.rest.dto.PlayRequest;
import com.slava.game.adapters.in.rest.dto.PlayResponse;
import com.slava.game.application.port.in.PlayHumanVsAiUseCase;
import com.slava.game.application.port.in.PlayerStatisticsRetrievalUseCase;
import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RestController;

import static org.springframework.http.ResponseEntity.ok;

@RequiredArgsConstructor
@RestController
public class GamesController implements GamesApi {

    private final PlayHumanVsAiUseCase playHumanVsAiUseCase;
    private final PlayerStatisticsRetrievalUseCase statisticsRetrievalUseCase;
    private final RestMapper mapper;

    @Override
    public ResponseEntity<PlayResponse> play(PlayRequest playRequest) {
        var result = playHumanVsAiUseCase.play(playRequest.getPlayerId(), playRequest.getShape().name());
        return ok(new PlayResponse().gameResult(result));
    }

    @Override
    public ResponseEntity<GetStatisticsResponse> getStatistics(Long playerId) {
        return ok(
            mapper.playerStatisticToGetStatisticsResponse(statisticsRetrievalUseCase.getStatistic(playerId))
        );
    }
}
