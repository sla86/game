package com.slava.game.adapters.out.persistence;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

//TODO: check how to use rest data with swagger
@Repository
public interface PlayerStatisticRepository extends CrudRepository<PlayerStatisticEntity, Long> {

}
