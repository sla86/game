package com.slava.game.adapters.out.persistence;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.hibernate.annotations.Immutable;

@Entity
@Immutable
@Table(name = "v_game_stats")
@NoArgsConstructor
@AllArgsConstructor
@Builder
@Getter
@Setter
public final class PlayerStatisticEntity {

    @Id
    private Long playerId;
    private Long games;
    private Long wins;
    private Long losts;
    private Long draws;
}
