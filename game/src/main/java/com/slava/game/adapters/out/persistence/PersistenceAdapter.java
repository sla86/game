package com.slava.game.adapters.out.persistence;

import java.util.Optional;

import com.slava.game.application.port.out.PersistencePort;
import com.slava.game.domain.Game;
import com.slava.game.domain.PlayerStatistic;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
public final class PersistenceAdapter implements PersistencePort {

    private final GameRepository gameRepository;
    private final PersistenceMapper mapper;
    private final PlayerStatisticRepository statisticRepository;

    @Override
    public void save(Game game) {
        var entity = mapper.gameToGameEntity(game);
        gameRepository.save(entity);
    }

    @Override
    public Optional<PlayerStatistic> getStats(Long playerId) {
        return statisticRepository.findById(playerId)
            .map(mapper::playerStatisticEntityToPlayerStatistic);
    }

}
