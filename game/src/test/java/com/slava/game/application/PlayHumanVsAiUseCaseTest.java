package com.slava.game.application;

import com.slava.game.application.port.out.GiftNotification;
import com.slava.game.application.port.out.PersistencePort;
import com.slava.game.application.service.PlayHumanVsAiService;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.doNothing;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
class PlayHumanVsAiUseCaseTest {

    @Mock
    PersistencePort persistencePort;
    @Mock
    GiftNotification giftNotification;

    PlayHumanVsAiService useCase;

    @BeforeEach
    void setUp() {
        useCase = new PlayHumanVsAiService(persistencePort, giftNotification);
    }

    @Test
        //for debuging
    void testPlayGame() {
        doNothing().when(persistencePort).save(any());

        var result = useCase.play(1L, "ROCK");

        assertNotNull(result);
    }
}