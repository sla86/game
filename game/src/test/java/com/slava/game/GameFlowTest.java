package com.slava.game;

import java.util.Random;

import com.slava.game.adapters.in.rest.dto.GetStatisticsResponse;
import com.slava.game.adapters.in.rest.dto.PlayRequest;
import com.slava.game.adapters.in.rest.dto.PlayResponse;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.boot.test.web.server.LocalServerPort;
import org.springframework.jdbc.core.JdbcTemplate;

import static com.slava.game.adapters.in.rest.dto.PlayRequest.ShapeEnum.PAPER;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;

@ITTestDefinition
class GameFlowTest {

    @LocalServerPort
    int port;

    @Autowired
    TestRestTemplate restTemplate;

    @Autowired
    JdbcTemplate jdbcTemplate;

    @BeforeEach
    void setUp() {
        var count = jdbcTemplate.queryForObject("select count(*) from t_game", Long.class);
        assertEquals(0, count);
    }

    @AfterEach
    void cleanUp() {
        jdbcTemplate.execute("delete from t_game");
    }

    @Test
    void playGameFlow() {
        var playerId = new Random().nextLong(1, Long.MAX_VALUE);

        playGame(playerId);

        checkStatistics(playerId);
    }

    private void playGame(long playerId) {
        //Given
        var playRequest = new PlayRequest()
            .playerId(playerId)
            .shape(PAPER);

        //When
        var playResponse = restTemplate.postForEntity(
            "http://localhost:" + port + "/gambling/api/v1/games",
            playRequest,
            PlayResponse.class);

        //Then
        assertEquals(200, playResponse.getStatusCodeValue());
        assertNotNull(playResponse.getBody().getGameResult());
        var count = jdbcTemplate.queryForObject("select count(*) from t_game"
                                                + " where " + playerId + " in (one_player_id, another_player_id)",
                                                Long.class);
        assertEquals(1, count);
    }

    private void checkStatistics(Long playerId) {
        //Given
        //game played

        //When
        var statisticsResponse = restTemplate.getForEntity(
            "http://localhost:" + port + "/gambling/api/v1//games/{playerId}/statistics",
            GetStatisticsResponse.class,
            playerId);

        //Then
        assertEquals(200, statisticsResponse.getStatusCodeValue());
        assertNotNull(statisticsResponse.getBody());
        assertEquals(playerId, statisticsResponse.getBody().getPlayerId());
        var count = jdbcTemplate.queryForObject("select count(*) from v_game_stats"
                                                + " where " + playerId + " = player_id",
                                                Long.class);
        assertEquals(1, count);
    }
}
