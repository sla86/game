package com.slava.game.adapters.out.persistence;

import com.slava.game.ITTestDefinition;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.test.context.ActiveProfiles;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;

@ITTestDefinition
class GameRepositoryTest {

    @Autowired
    GameRepository gameRepository;

    @Autowired
    JdbcTemplate jdbcTemplate;

    @BeforeEach
    void setUp() {
        var count = jdbcTemplate.queryForObject("select count(*) from t_game", Long.class);
        assertEquals(0, count);
    }

    @AfterEach
    void cleanUp() {
        jdbcTemplate.execute("delete from t_game");
    }

    @Test
    void testWriting() {
        var gameEntity = GameEntity.builder()
            .onePlayerId(1L)
            .onePlayerShape("a")
            .anotherPlayerId(2L)
            .anotherPlayerShape("b")
            .winerPlayerId(1L)
            .build();

        gameRepository.save(gameEntity);

        var count = jdbcTemplate.queryForObject("select count(*) from t_game", Long.class);
        assertEquals(1, count);
    }

    @Test
    void testReading() {
        jdbcTemplate.execute(
            "insert into t_game (another_player_id, another_player_shape, one_player_id, one_player_shape, play_datetime, winer_player_id, id) "
            + "values (1, 'a', 2, 'b', now(), 1, 1)");

        var gameEntity = gameRepository.findById(1L);

        assertNotNull(gameEntity);
    }

}