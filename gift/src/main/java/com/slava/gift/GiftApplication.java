package com.slava.gift;

import com.slava.rabbitmq.infra.RabbitmqExchangeConfiguration;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Import;

@SpringBootApplication
@Import(RabbitmqExchangeConfiguration.class)
public class GiftApplication {

	public static void main(String[] args) {
		SpringApplication.run(GiftApplication.class, args);
	}

}
