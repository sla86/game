package com.slava.gift.application.in;

public interface GiftRequestUseCase {
    void giveGift(Long playerId);
}
