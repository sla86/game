package com.slava.gift.application.service;

import com.slava.gift.application.in.GiftRequestUseCase;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

@Service
@Slf4j
public class GiftRequestService implements GiftRequestUseCase {

    @Override
    public void giveGift(Long playerId) {
        log.debug("Received gift request for user [{}]", playerId);
    }
}
