package com.slava.gift.adapters.in.messaging;

import java.util.function.Consumer;

import com.slava.gift.application.in.GiftRequestUseCase;
import com.slava.proto.game.GiftRequest;
import lombok.RequiredArgsConstructor;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
@RequiredArgsConstructor
public class GiftRequestConsumer {

    private final GiftRequestUseCase giftRequestUseCase;

    @Bean
    Consumer<GiftRequest> giftRequestIn() {
        return giftRequest -> giftRequestUseCase.giveGift(giftRequest.getPlayerid());
    }
}
