package com.slava.atf;

import java.time.Duration;
import java.time.Instant;

import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.web.reactive.function.client.WebClient;
import reactor.core.publisher.Mono;

import static org.springframework.http.HttpHeaders.CONTENT_TYPE;
import static org.springframework.http.MediaType.APPLICATION_JSON_VALUE;

@SpringBootTest
class AtfApplicationTests {

    WebClient webClient = WebClient.create("http://localhost:8080");

    @Test
    void contextLoads() {
        WebClient.RequestHeadersSpec<?> spec = webClient.post()
            .uri("/gambling/api/v1/games")
            .header(CONTENT_TYPE, APPLICATION_JSON_VALUE)
            .body(Mono.just("{ \"playerId\": 123, \"shape\": \"PAPER\"}"), String.class);

        var start = Instant.now();

        for (int i = 0; i < 1000; i++) {
            String result = spec
                .retrieve()
                .bodyToMono(String.class)
                .block();
            System.out.println(result);
        }

        var end = Instant.now();
        System.out.println(Duration.between(start, end).toSeconds());
    }
}
